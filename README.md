This is an example project to add gitlab badges for a kotlin maven springboot project 
Things to note in this porject 
- Jacoco maven plugin settings in `pom.xml`
- configuration of artifacts and reports in `.gitlab-ci.yml` file 
- publishing jacoco report as gitlab pages, which can then be used as link on coverage badge 
- Regex for coverage in `.gitlab-ci.yml` which reads based on `cat` command I have used to log jacoco code coverage into the build logs
