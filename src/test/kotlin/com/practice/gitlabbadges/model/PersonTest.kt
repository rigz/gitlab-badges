package com.practice.gitlabbadges.model

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class PersonTest{
    @Test
    fun `test object creation`() {
        val person = Person("john", 11)
        assertEquals(person, Person("john",11))
    }
    @Test
    fun `test toString method`(){
        val person = Person("marry",10)
        assertNotNull(person.toString())
    }
    @Test
    fun `random test`(){
        val person = Person("abc",1)
        assertEquals(1, person.age)
        assertEquals("abc",person.name)
    }
}