package com.practice.gitlabbadges

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GitlabBadgesApplication

fun main(args: Array<String>) {
    runApplication<GitlabBadgesApplication>(*args)
}
