package com.practice.gitlabbadges.model

data class Person(val name: String, val age: Int)
